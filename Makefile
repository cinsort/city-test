RUN_COMPOSER_COMMAND:=docker run --rm --interactive --tty --workdir /app --volume $(shell pwd)/server/main-service:/app:rw \
	--user $(shell id -u):$(shell id -g) composer --ignore-platform-reqs --no-interaction

prepare-env:
	echo "preparing" && \
	test -f .env || cp .env.example .env

composer-install: prepare-env
	${RUN_COMPOSER_COMMAND} install
composer-update: prepare-env
	${RUN_COMPOSER_COMMAND} update

pull: prepare-env
	docker-compose pull database
build: prepare-env composer-install
	docker-compose build main-service
up: build pull
	docker-compose up -d

#run with "make create-cities n=5"
create-cities:
	docker-compose run --rm --no-deps main-service php artisan create:cities count=$(n)