<?php

return [
    'get_addresses_url' => 'https://cleaner.dadata.ru/api/v1/clean/address',
    'token' => (string) env('DADATA_TOKEN'),
    'secret' => (string) env('DADATA_SECRET'),
];