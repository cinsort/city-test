<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

class EmptyAnswerFromServiceByUrlException extends Exception
{

    /**
     * @var int
     */
    protected $code = 500;

    public static function make(string $url): self
    {
        $exception = new static();
        $exception->message = "Empty answer from request by $url url!";

        return $exception;
    }

}
