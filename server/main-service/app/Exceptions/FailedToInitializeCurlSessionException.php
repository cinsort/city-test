<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

class FailedToInitializeCurlSessionException extends Exception
{

    /**
     * @var string
     */
    protected $message = 'Failed to initialize curl session exception!';

    /**
     * @var int
     */
    protected $code = 500;

}
