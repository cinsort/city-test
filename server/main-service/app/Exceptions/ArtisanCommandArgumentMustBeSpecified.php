<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

class ArtisanCommandArgumentMustBeSpecified extends Exception
{

    /**
     * @var int
     */
    protected $code = 500;

    public static function make(string $argumentName): self
    {
        $exception = new static();
        $exception->message = "Artisan command argument $argumentName must be specified!";

        return $exception;
    }

}
