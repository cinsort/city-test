<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

class UnexpectedAnswerFromServiceByUrlException extends Exception
{

    /**
     * @var int
     */
    protected $code = 500;

    public static function make(string $url, string $answer): self
    {
        $exception = new static();
        $exception->message = "Unexpected answer from request by $url url : $answer!";

        return $exception;
    }

}
