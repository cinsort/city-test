<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Exceptions\ArtisanCommandArgumentMustBeSpecified;
use App\Helpers\Request;
use App\Models\City;
use App\Models\FederalDistrict;
use App\Models\Region;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CreateCitiesCommand extends Command
{

    /**
     * @var string
     */
    protected $signature = 'create:cities {count : Amount of cities to create }';

    /**
     * @var string
     */
    protected $description = 'Adds a new cities with specified amount to the system';

    /**
     * @return array
     */
    protected function getArguments(): array
    {
        $callback = function (array $arguments, string $argument) {
            if (!preg_match('/^(.+)=(.+)$/', $argument, $matches)) {
                return $arguments;
            }

            $arguments[$matches[1]] = $matches[2];

            return $arguments;
        };

        return array_reduce($this->arguments(), $callback, []);
    }

    public function handle(): void
    {
        $this->info('Start processing...');

        $countCities = (int)$this->getArguments()['count']
            ?? throw ArtisanCommandArgumentMustBeSpecified::make('count');

        foreach (range(1, $countCities) as $count) {
            $randomCityName = Arr::random(config('cities'));

            $cityOptions = (new Request())->getAddressInfo($randomCityName);

            $federalDistrictName = $cityOptions['federal_district'] ?? false;
            if (!$federalDistrictName) {
                $this->info("No available information found for $randomCityName city federal district");
                continue;
            }

            $regionName = $cityOptions['region'] ?? false;

            if (!$regionName) {
                $this->info("No available information found for $randomCityName city region");
                continue;
            }

            $federalDistrict = $this->processFederalDistrict($federalDistrictName);
            $region = $this->processRegion($regionName, $federalDistrict->id);
            $this->processCity($randomCityName, $region->id);
        }

        $this->info('Finished processing.');

    }

    /**
     * Find federal district or create it if not exists
     *
     * @param string $name
     * @return FederalDistrict
     */
    private function processFederalDistrict(string $name): FederalDistrict
    {
        $federalDistrict = FederalDistrict::find($name);

        if ($federalDistrict !== null) {
            return $federalDistrict;
        }

        $federalDistrict = new FederalDistrict(['id' => $name]);
        $federalDistrict->save();

        $this->info("Created federal district $name with attributes:");
        dump($federalDistrict->getAttributes());

        return $federalDistrict;
    }

    /**
     * Find region or create it if not exists
     *
     * @param string $name
     * @param string $federalDistrictId
     * @return Region
     */
    private function processRegion(string $name, string $federalDistrictId): Region
    {
        $region = Region::find($name);

        if ($region !== null) {
            return $region;
        }

        $region = new Region();
        $region->id = $name;
        $region->federal_district_id = $federalDistrictId;

        $region->save();

        $this->info("Created region $name with attributes:");
        dump($region->getAttributes());

        return $region;
    }

    /**
     * Find city or create it if not exists
     *
     * @param string $name
     * @param string $regionId
     * @return City
     */
    private function processCity(string $name, string $regionId): City
    {
        $city = City::find($name);

        if (City::find($name) !== null) {
            $this->info("City $name already exists");

            return $city;
        }

        $city = new City();
        $city->id = $name;
        $city->region_id = $regionId;

        $city->save();

        $this->info("Created city $name with attributes:");
        dump($city->getAttributes());

        return $city;
    }

}
