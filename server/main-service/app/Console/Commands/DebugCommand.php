<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DebugCommand extends Command
{

    /**
     * @var string
     */
    protected $signature = 'debug';

    public function handle(): void
    {

    }

}
