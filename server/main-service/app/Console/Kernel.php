<?php

declare(strict_types=1);

namespace App\Console;

use App\Console\Commands\CreateCitiesCommand;
use App\Console\Commands\DebugCommand;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        DebugCommand::class,
        CreateCitiesCommand::class
    ];

}
