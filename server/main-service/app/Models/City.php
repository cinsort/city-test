<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    protected $guarded = [];

    protected $table = 'cities';

    protected $keyType = 'string';

    public $timestamps = false;

}
