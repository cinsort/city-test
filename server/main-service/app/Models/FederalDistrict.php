<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FederalDistrict extends Model
{

    protected $guarded = [];

    protected $keyType = 'string';

    public $timestamps = false;


    /**
     * Set relation on model to City model. Used for retrieving and saving related model attributes.
     */
    public function regions(): HasMany
    {
        return $this->hasMany(Region::class, 'federal_distinct_id', 'id');
    }

}
