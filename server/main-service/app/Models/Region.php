<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Region extends Model
{

    protected $guarded = [];

    protected $keyType = 'string';

    public $timestamps = false;

    /**
     * Set relation on model to City model. Used for retrieving and saving related model attributes.
     */
    public function cities(): HasMany
    {
        return $this->hasMany(City::class, 'region_id', 'id');
    }

}
