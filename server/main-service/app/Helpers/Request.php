<?php

namespace App\Helpers;

use App\Exceptions\EmptyAnswerFromServiceByUrlException;
use App\Exceptions\FailedToInitializeCurlSessionException;
use App\Exceptions\UnexpectedAnswerFromServiceByUrlException;

class Request
{

    private readonly string $addressUrl;

    private readonly string $token;

    private readonly string $secret;

    private \CurlHandle $curlInstance;

    /**
     * @throws FailedToInitializeCurlSessionException
     */
    public function __construct()
    {
        $this->addressUrl = config('dadata.get_addresses_url');
        $this->token = config('dadata.token');
        $this->secret = config('dadata.secret');

        $this->curlInstance = curl_init() ?: throw new FailedToInitializeCurlSessionException();
    }


    /**
     * Get the specified address information from dadata service
     *
     * @throws UnexpectedAnswerFromServiceByUrlException
     */
    public function getAddressInfo(string $address): array
    {
        curl_setopt_array($this->curlInstance, array(
            CURLOPT_URL => $this->addressUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '[ "' . $address . '" ]',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Token ' . $this->token,
                'X-Secret: ' . $this->secret
            ),
        ));

        $response = curl_exec($this->curlInstance);

        curl_close($this->curlInstance);
        return json_decode($response, true)[0]
            ?? throw UnexpectedAnswerFromServiceByUrlException::make($this->addressUrl, $response);
    }

}