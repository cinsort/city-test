# Description
In This project we have 3 Models (City, FederalDistrict, Region) and created migrations for them.
By running artisan command for cities creating user's able to fill database with data
from dadata service.

## Deploy

Firstly, to be able to get cities info you need to set those variables via `.env.example` file

```shell
DADATA_TOKEN="PASTE_YOUR_API_KEY_HERE"
DADATA_SECRET="PASTE_YOUR_SECRET_KEY_HERE"
```

To locally deploy project run from corner dir:

```shell
make up
```


> If not able to use make, copy env.example to .env, install composer requirements and run:
> 
> ```shell
>docker-compose up -d
>```

## Process cities creation

To locally process cities creation firstly deploy project, then run:

```shell
make create-cities n=25
```

> If not able to use make, run:
>
> ```shell
> docker-compose up -d && docker-compose run --rm --no-deps main-service php artisan create:cities count=$(n)
>```